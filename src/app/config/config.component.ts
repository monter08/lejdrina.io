import { Component, OnInit, Output } from '@angular/core';
import {Substitute} from '../substitute';
import {SubstituteService} from '../substitute.service';

import {BluetoothService} from './../ble/bluetooth.service';



@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  substitutes: Substitute[];
  i: number;

  constructor(public service: SubstituteService, public ble: BluetoothService) {
    this.i = 7;
  }

  ngOnInit() {
    const substitutesObservable = this.service.getSubstitutes();
    substitutesObservable.subscribe((substitutesData: Substitute[]) => {
      this.substitutes = substitutesData;
    });
    // console.log(this.ble.device);
  }

  // Update action
  prevValue = null;

  saveValue(value) {
    this.prevValue = value;
  }

  processChange(value) {
    if (value !== this.prevValue) {
      this.ble.updateSubstitute(this.substitutes);
    }
  }

  edit() {
    console.log("Waiting to data.");
  }


}
