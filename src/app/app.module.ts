import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { WebBluetoothModule } from '@manekinekko/angular-web-bluetooth';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ConfigComponent } from './config/config.component';


import {BluetoothComponent} from './ble/bluetooth.compontent';
import {BluetoothService} from './ble/bluetooth.service';

import { FormsModule } from '@angular/forms';
import { SubstituteService } from './substitute.service';
import { BleService } from './ble.service';
import { SubstituteComponent } from './substitute/substitute.component';
import {DrinkService} from './drink.service';
import { AddDrinkComponent } from './add-drink/add-drink.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ConfigComponent,
    BluetoothComponent,
    SubstituteComponent,
    AddDrinkComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    WebBluetoothModule.forRoot({
      enableTracing: true // or false, this will enable logs in the browser's console
    })
  ],
  providers: [BleService, SubstituteService, BluetoothService, DrinkService],
  bootstrap: [AppComponent]
})

export class AppModule { }
