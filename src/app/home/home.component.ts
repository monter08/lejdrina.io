import { Component, OnInit, OnDestroy } from '@angular/core';
import {Drink} from '../drink';
import {DrinkService} from '../drink.service';
import {BluetoothService} from './../ble/bluetooth.service';
import {Substitute} from '../substitute';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  drinks: Drink[];
  substitutes: Substitute[];

  constructor(public service: DrinkService, private ble: BluetoothService) {}

  ngOnInit() {
    const drinksObservable = this.service.getDrinks();
    drinksObservable.subscribe((drinksData: Drink[]) => {
      this.drinks = drinksData;
    });

  }


}
