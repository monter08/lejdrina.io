import { Injectable } from '@angular/core';
import {Substitute} from './substitute';
import {Drink} from './drink';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DrinkService {

  drinks: Drink[] = [];

  constructor() {

    const d1 = new Drink({
      name: 'Paszko Drink',
      img: 'assets/photo/longisland.jpg',
    });

    const d2 = new Drink({
      name: 'Mocny Hass',
      img: 'assets/photo/longisland.jpg',
    });

    const d3 = new Drink({
      name: 'Dawidzior++',
      img: 'assets/photo/longisland.jpg',
    });
    const p1 = new Substitute( 'Janusz Whisky', 1000, 40);
    const p4 = new Substitute('Paszko Vodka', 1000, 40);
    d1.addSubstitute(p1, 50);
    d2.addSubstitute(p1, 100);
    d3.addSubstitute(p4, 100);

    this.drinks = [d1, d2, d3];
  }

  public getDrinks(): any {
    const drinksObservable = new Observable(observer => {
      observer.next(this.drinks);
    });

    return drinksObservable;
  }


  setJson(){

  }

}
