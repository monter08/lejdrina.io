import {Substitute} from './substitute';

export class Drink {
    id: number;
    name: string;
    img: string;
    substitutes: object[];


    constructor(values = {}) {
        Object.assign(this, values);
        this.substitutes = [];
    }
    addSubstitute(sub: Substitute, mlV){
        this.substitutes.push({substitute: sub, ml: mlV});
    }
}
