import { Component, OnInit, Output } from '@angular/core';
import {Substitute} from '../substitute';
import {SubstituteService} from '../substitute.service';


@Component({
  selector: 'app-substitute',
  templateUrl: './substitute.component.html',
  styleUrls: ['./substitute.component.css']
})
export class SubstituteComponent implements OnInit {

  substitutes: Substitute[];


  constructor(public service: SubstituteService) {}

  ngOnInit() {
    const substitutesObservable = this.service.getSubstitutes();
    substitutesObservable.subscribe((substitutesData: Substitute[]) => {
      this.substitutes = substitutesData;
    });
  }

}
