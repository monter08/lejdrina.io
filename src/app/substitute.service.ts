import { Injectable, Output} from '@angular/core';
import {Substitute} from './substitute';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class SubstituteService {

  substitutes: Substitute[] = [
    {
      name: 'Janusz Whisky',
      ml: 1000,
      percent: 40
    },
    {
      name: 'Paszko Vodka',
      ml: 500,
      percent: 40
    }];


  constructor() { }

  public getSubstitutes(): any {
    const substitutesObservable = new Observable(observer => {
        observer.next(this.substitutes);
    });

    return substitutesObservable;
  }

}
