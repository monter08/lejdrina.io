import { Injectable } from '@angular/core';
import { BluetoothCore, BrowserWebBluetooth, ConsoleLoggerService } from '@manekinekko/angular-web-bluetooth';
import { map } from 'rxjs/operators';
import { BleService } from '../ble.service';
import {DrinkService} from '../drink.service';
import {SubstituteService} from '../substitute.service';
import {Drink} from './../drink';
import {Substitute} from '../substitute';



@Injectable({
  providedIn: 'root',
})


export class BluetoothService {

  drinks: Drink[];
  substitutes: Substitute[];

  constructor(public DrinkServ: DrinkService, public readonly ble: BluetoothCore, public SubstituteServ: SubstituteService) {
  	// this.ble = ble;
  	// this.value();


    // service.config({
    //   decoder: (value: DataView) => value.getInt8(0),
    //   service: "ef680200-9b35-4933-9b10-52ffa9740042",
    //   characteristic: "ef680203-9b35-4933-9b10-52ffa9740042"
    // })


    const drinksObservable = this.DrinkServ.getDrinks();
    drinksObservable.subscribe((drinksData: Drink[]) => {
      this.drinks = drinksData;
    });



     const substitutesObservable = this.SubstituteServ.getSubstitutes();
      substitutesObservable.subscribe((substitutesData: Substitute[]) => {
        this.substitutes = substitutesData;
    });
  }

  ngInit(){
    console.log('halu');

  }

  getDevice() {
    // call this method to get the connected device
    return this.ble.getDevice$();
  }


  get device() {
    return this.ble.getDevice$();
  }
  public service = 'battery_service';
  public characteristic = 'battery_level';
  public outputvalue = '';
  public lastupdate = null;
  public connect_service = null;
  public connect_device = null;
  private connect_characteristic = null;

   public connect() {
	    var self = this;
	    var nav: any = navigator;
	    if (nav.bluetooth && nav.bluetooth.requestDevice) {
	      nav.bluetooth.requestDevice({ filters: [{ services: [self.service] }] })
	        .then(device => {
	          console.log("requestDevice", device);
	          /* ... */
	          console.log("device name", device.name);
	          this.connect_device = device;

	          return device.gatt.connect();
	        })  .catch(error => {
    console.log('Argh! ' + error);
  })
	        .then(server => {
	          console.log("server", server);
	          // Getting Battery Service...
	          return server.getPrimaryService(self.service);
	        })  .catch(error => {
    console.log('Argh! ' + error);
  })
	        .then(service => {
	          console.log("service", service);
	          this.connect_service = service;

	          // Getting Characteristic...
	          return service.getCharacteristic(self.characteristic);
	        })
  .catch(error => {
    console.log('Argh! ' + error);
  })

          .then(characteristic => {
	          console.log("characteristic", characteristic);
	          // Reading Battery Level...
            var val = '{"substitute":[{"id":1,"name":"Janusz Whisky33","ml":1000,"percent":40},{"id":4,"name":"Paszko Vodka","ml":500,"percent":40}],"drinks":[{"id":1,"name":"Paszko Drink","img":"assets/photo/longisland.jpg","substitutes":[{"substitute":{"id":1,"name":"Janusz Whisky","ml":1000,"percent":40},"ml":50}]},{"id":2,"name":"Mocny Hass","img":"assets/photo/longisland.jpg","substitutes":[{"substitute":{"id":1,"name":"Janusz Whisky","ml":1000,"percent":40},"ml":100}]},{"id":3,"name":"Dawidzior++","img":"assets/photo/longisland.jpg","substitutes":[{"substitute":{"id":1,"name":"Paszko Vodka","ml":1000,"percent":40},"ml":100}]}]}';
             return characteristic.writeValue(new TextEncoder().encode(val));

	          this.connect_characteristic = characteristic;
	          // return characteristic.readValue();
	        })
          .catch(error => { console.log("webbluetooth error", error); })
	        .then(value => {
	          console.log("value", new TextDecoder().decode(value));
	        });
	       

	    }
	}





  stream() {
    // call this method to get a stream of values emitted by the device for a given characteristic
    return this.ble.streamValues$().pipe(
      map((value: DataView) => value.getInt8(0))
    );
  }

  isConnect(){
  	return this.connect_service !== null;
  }

  value() {
  	if(!this.isConnect())
  		console.log('nie połączony');
  }

  updateSubstitute(data){
    this.write(JSON.stringify({substitute: data, drinks: this.drinks}));
  }

  write(val){
    // if(!this.isConnect())
    //   return false;
  	// let value = Uint8Array.of(val);
    console.log(val);
    val = '{"substitute":[{"id":1,"name":"Janusz Whisky33","ml":1000,"percent":40},{"id":4,"name":"Paszko Vodka","ml":500,"percent":40}],"drinks":[{"id":1,"name":"Paszko Drink","img":"assets/photo/longisland.jpg","substitutes":[{"substitute":{"id":1,"name":"Janusz Whisky","ml":1000,"percent":40},"ml":50}]},{"id":2,"name":"Mocny Hass","img":"assets/photo/longisland.jpg","substitutes":[{"substitute":{"id":1,"name":"Janusz Whisky","ml":1000,"percent":40},"ml":100}]},{"id":3,"name":"Dawidzior++","img":"assets/photo/longisland.jpg","substitutes":[{"substitute":{"id":1,"name":"Paszko Vodka","ml":1000,"percent":40},"ml":100}]}]}';
    this.connect_characteristic.writeValue(new TextEncoder().encode(val));


  	// return this.connect_characteristic.getDescriptor('gatt.characteristic_user_description').then(function(descriptor){
  	// 	descriptor.writeValue(0);
  	// })

  }


}
