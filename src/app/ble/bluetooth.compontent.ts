import { Component, OnInit } from '@angular/core';
import { BluetoothCore, BrowserWebBluetooth, ConsoleLoggerService } from '@manekinekko/angular-web-bluetooth';
import { Subscription } from 'rxjs';
import { BleService } from '../ble.service';

// make sure we get a singleton instance of each service
const PROVIDERS = [{
  provide: BluetoothCore,
  useFactory: (b, l) => new BluetoothCore(b, l),
  deps: [BrowserWebBluetooth, ConsoleLoggerService]
}, {
  provide: BleService,
  useFactory: (b) => new BleService(b),
  deps: [BluetoothCore]
}];

@Component({
  selector: 'ble-manager',
  template: '',
  styles: [''],
  providers: PROVIDERS
})
export class BluetoothComponent implements OnInit {
  // value = null;
  // mode = "determinate";
  // color = "primary";


  valuesSubscription: Subscription;
  streamSubscription: Subscription;
  deviceSubscription: Subscription;

  get device() {
    return this.service.getDevice();
  }

  constructor(
    public service: BleService) {

    service.config({
      decoder: (value: DataView) => value.getInt8(0),
      service: "battery_service",
      characteristic: "battery_level"
    })
  }

  ngOnInit() {
    this.getDeviceStatus();

    this.streamSubscription = this.service.stream()
      .subscribe(this.updateValue.bind(this), this.hasError.bind(this));

  }

  getDeviceStatus() {
    this.deviceSubscription = this.service.getDevice()
      .subscribe(device => {
        console.log(device);
      }, this.hasError.bind(this));
  }

  requestValue() {
    this.valuesSubscription = this.service.value()
      .subscribe(null, this.hasError.bind(this));
  }

  updateValue(value: number) {
    console.log('Reading battery level %d', value);
    // this.value = value;
    // this.mode = "determinate";
  }

  disconnect() {
    this.service.disconnectDevice();
    this.deviceSubscription.unsubscribe();
    this.valuesSubscription.unsubscribe();
  }

  hasError(error: string) {
    // console.log(e)
    alert(error);
    // this.snackBar.open(error, 'Close');
  }

  // ngOnDestroy() {
  //   this.valuesSubscription.unsubscribe();
  //   this.deviceSubscription.unsubscribe();
  //   this.streamSubscription.unsubscribe();
  // }
}


