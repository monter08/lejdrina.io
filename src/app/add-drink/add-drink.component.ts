import { Component, OnInit } from '@angular/core';
import {Substitute} from '../substitute';
import {SubstituteService} from '../substitute.service';
import {Drink} from '../drink';
import {DrinkService} from '../drink.service';

@Component({
  selector: 'app-add-drink',
  templateUrl: './add-drink.component.html',
  styleUrls: ['./add-drink.component.css']
})

export class AddDrinkComponent implements OnInit {

  substitutes: Substitute[];
  drinks: Drink[];

  constructor(public serviceDrink: DrinkService, public service: SubstituteService) {
  }

  ngOnInit() {
    const substitutesObservable = this.service.getSubstitutes();
    substitutesObservable.subscribe((substitutesData: Substitute[]) => {
      this.substitutes = substitutesData;
    });
    const drinksObservable = this.serviceDrink.getDrinks();
    drinksObservable.subscribe((drinksData: Drink[]) => {
      this.drinks = drinksData;
    });
  }

  addDrink(){
  }

  editDrink(selectedDrink){
    console.log(selectedDrink);
    // const nameDrink = selectedDrink.getAttribute('attr.drink-name');
    // console.log(nameDrink);
  }


}
